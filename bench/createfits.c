#include "fits-helper.h"
#include <fitsio.h>
#include <longnam.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  fitsfile *fptr;
  long naxes[4];
  int naxis;
  float *pix;
  int status = 0;

  printf("argc %d\n", argc);
  for (int i = 0; i < argc; i++)
    printf("%s\n", argv[i]);

  if (argc < 2) {
    printf("Usage ./createfits <infile> <outfile>\n");
    return 1;
  }

  if (fits_open_image(&fptr, argv[1], READONLY, &status)) {
    return 1;
  }

  char *out_file = argv[2];

  fits_data_t *fits = malloc(sizeof(fits_data_t));

  extract_data_from_fits(fptr, fits);

  for (int channel = 0; channel < fits->spec_size; channel++) {
    float *plane = fits->data + (channel * fits->spat_size);
    for (int pix = 0; pix < fits->spat_size; pix++) {
      plane[pix] = (channel+1);
    }
  }

  store_data_copying_fits_header(fptr, out_file, fits);

  free(fits);
  fits_close_file(fptr, &status);
}
