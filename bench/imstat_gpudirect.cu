#include <algorithm>

#include <cmath>
#include <cstddef>

#include <cufile.h>
#include <fcntl.h>
#include <fstream>
#include <limits>
#include <sys/types.h>
#include <unistd.h>

#include "fits-io-gpudirect.hh"
#include "fits-io.hh"

#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

#include <thrust/count.h>
#include <thrust/functional.h>
#include <thrust/transform_reduce.h>

struct not_nan {
  __host__ __device__ bool operator()(float x) { return !isnan(x); }
};

struct replace_nan_op : public thrust::unary_function<float, float> {
  __host__ __device__ float operator()(float x) { return isnan(x) ? 0.0f : x; }
};

struct stats_data {
  int count;
  float mean;
  float min;
  float max;
  float m2;

  void init() {
    count = 0;
    mean = min = max = m2 = 0.0;
    min = std::numeric_limits<float>::max();
    max = std::numeric_limits<float>::min();
  }

  float variance() { return m2 / (float)count; }
};

struct stat_data_unary_op {
  __host__ __device__ stats_data operator()(const float &x) const {
    stats_data res;
    if (isnan(x)) {
      res.count = 0;
      res.min = std::numeric_limits<float>::max();
      res.max = std::numeric_limits<float>::min();
      res.m2 = 0;
    } else {
      res.count = 1;
      res.min = x;
      res.max = x;
      res.mean = x;
      res.m2 = 0;
    }
    return res;
  }
};

struct stats_data_binary_op
    : public thrust::binary_function<const stats_data &,
                                     const stats_data &,
                                     stats_data> {
  __host__ __device__ stats_data operator()(const stats_data &x,
                                            const stats_data &y) {
    stats_data res;

    if (x.count == 0) {
      res.count = y.count;
      res.min = y.min;
      res.max = y.max;
      res.mean = y.mean;
      res.m2 = y.m2;
    } else if (y.count == 0) {
      res.count = x.count;
      res.min = x.min;
      res.max = x.max;
      res.mean = x.mean;
      res.m2 = x.m2;
    } else {
      int count = x.count + y.count;
      float delta = y.mean - x.mean;
      float delta2 = delta * delta;

      res.count = count;
      res.min = thrust::min(x.min, y.min);
      res.max = thrust::max(x.max, y.max);
      res.mean = x.mean + delta * y.count / count;

      res.m2 = x.m2 + y.m2;
      res.m2 += delta2 * x.count * y.count / count;
    }

    return res;
  }
};

int main(int argc, char **argv) {
  if (argc != 2) {
    std::cerr << "Usage: fits <fits-file>" << std::endl;
    return 1;
  }
  std::string filename(argv[1]);
  fits::FITS fits(filename);
  // fits.print_info();

  printf("#%7s %15s %10s %10s %10s %10s %10s %10s %10s\n", "Channel",
         "Frequency", "Mean", "Std", "Median", "MADFM", "1%ile", "Min", "Max");
  printf("#%7s %15s %10s %10s %10s %10s %10s %10s %10s\n", " ", "MHz",
         "mJy/beam", "mJy/beam", "mJy/beam", "mJy/beam", "mJy/beam", "mJy/beam",
         "mJy/beam");

  CUfileError_t status;
  status = cuFileDriverOpen();

  fits::FITSDataGPU data(fits.spat_size, 1, true);
  fits::GPUFileHandle file(filename, fits.data_position);

  fits::endian_swap_op<float> endian_swap;
  // replace_nan_op nanop;

  for (size_t channel = 0; channel < fits.spec_size; channel++) {
    ssize_t ret =
        file.readData(data, fits.spat_size, channel * fits.spat_size, 0);

    if (ret < 0 || ret != fits.spat_size * sizeof(float)) {
      fprintf(stderr, "Failed to read channel %zu\n", channel);
    }

    thrust::transform(thrust::device, data.raw.begin(), data.raw.end(),
                      data.raw.begin(), endian_swap);

    // thrust::transform(thrust::device, data.raw.begin(), data.raw.end(),
    //                   data.raw.begin(), nanop);

    stat_data_unary_op unary_op;
    stats_data_binary_op binary_op;
    stats_data init;

    init.init();

    stats_data res = thrust::transform_reduce(data.raw.begin(), data.raw.end(), unary_op, init, binary_op);

    res.mean *= 1000.0;
    res.min *= 1000.0;
    res.max *= 1000.0;
    float std = std::sqrt(res.variance());

    printf("%8zu %15.6f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f % 10.3f\n",
           (channel + 1), 1.0f, res.mean, std, 0.0f, 0.0f, 0.0f, res.min,
           res.max);
  }

  cuFileDriverClose();
}
