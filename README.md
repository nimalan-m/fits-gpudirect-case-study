# FITS GPU Direct Case Study

## Examples

```cpp
#include "fits-io.hh"
#include "fits-io-gpudirect.hh"

{
  CUfileError_t status;
  status = cuFileDriverOpen();

  fits::FITSDataGPU data(fits.spat_size, 1, true);
  fits::GPUFileHandle file(filename, fits.data_position);

  ssize_t ret =
        file.readData(data, fits.spat_size, channel * fits.spat_size, 0);
  
  // FITS content is bif endian transform to little endian
  thrust::transform(thrust::device, data.raw.begin(), data.raw.end(),
                      data.raw.begin(), op);
  
  // Replace NaN with 0
  thrust::transform(thrust::device, data.raw.begin(), data.raw.end(),
                    data.raw.begin(), nanop);

  sum = thrust::reduce(data.raw.begin(), data.raw.end(), 0.0,
                         thrust::plus<float>());

  cuFileDriverClose();
}

```

For more examples check the [bench](./bench/) folder

## Usage in external project

```
ExternalProject_Add(fits-gpudirect
    GIT_REPOSITORY https://gitlab.com/nimalan-m/fits-gpudirect-case-study.git
    SOURCE_DIR ${EXTERNAL_INSTALL_LOCATION}
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${EXTERNAL_INSTALL_LOCATION}
    SOURCE_SUBDIR fits-gpudirect
)

# 
target_link_libraries(${TARGET} PUBLIC
    # Other dependencies
    FitsGPUDirect # Accelerated library for reading FITS into GPU memory with GPUDirect
)
```

## Building

```
cmake -B "build" -S .
cmake --build build
```

## Limitations

1. As the case study is done for Nvidia GPU Direct only supports Nvidia GPUs

**FITS Parsing**

1. Can only parse 4D FITS images `[Ra] [Dec] [Frequency] [Polarisation]` with Polarisation axis = 1
2. Does not support bscaling in FITS
3. Cannot handle compressed FITS
