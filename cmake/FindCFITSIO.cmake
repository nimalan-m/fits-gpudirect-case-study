if(NOT CFITSIO_FOUND)
    find_path(CFITSIO_INCLUDE_DIR fitsio.h
        HINTS $ENV{CFITSIO_ROOT_DIR} 
        PATH_SUFFIXES include include/cfitsio)
    find_library(CFITSIO_LIBRARY cfitsio
        HINTS $ENV{CFITSIO_ROOT_DIR} 
        PATH_SUFFIXES lib)
  
    mark_as_advanced(CFITSIO_INCLUDE_DIR CFITSIO_LIBRARY)

    include(FindPackageHandleStandardArgs)
    find_package_handle_standard_args(CFITSIO DEFAULT_MSG
        CFITSIO_LIBRARY CFITSIO_INCLUDE_DIR)

    set(CFITSIO_INCLUDE_DIRS ${CFITSIO_INCLUDE_DIR})
    set(CFITSIO_LIBRARIES ${CFITSIO_LIBRARY})

endif(NOT CFITSIO_FOUND)