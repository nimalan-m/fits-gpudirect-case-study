#include <cufile.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>

#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>

#include "fits-io-gpudirect.hh"
#include "fits-io.hh"

namespace fits {

// FIXME: Revisit object lifecycle
GPUFileHandle::GPUFileHandle(std::string filename,
                             std::ios::pos_type data_position)
    : filename(filename), data_position(data_position) {

  fd = open(filename.c_str(), O_RDONLY | O_DIRECT);
  if (fd < 0) {
    // Do I need to print as I'm already returning a value
    std::cerr << "Unable to open file in direct mode" << std::endl;
    return;
    // TODO: Should I throw an exception here?
    // return UNABLE_TO_OPEN_FILE_IN_DIRECT_MODE;
  }

  // Describe in handle to use this file
  memset((void *)&cf_desc, 0, sizeof(CUfileDescr_t));
  cf_desc.handle.fd = fd;
  cf_desc.type = CU_FILE_HANDLE_TYPE_OPAQUE_FD;

  // Register handle
  status = cuFileHandleRegister(&cf_handle, &cf_desc);
  if (status.err != CU_FILE_SUCCESS) {
    fprintf(stderr, "Unable to register handle\n");
    close(fd);
    return;
    // TODO: Should I throw an exception here?
    // return UNABLE_TO_REGISTER_CU_HANDLER;
  }
}

GPUFileHandle::~GPUFileHandle() {
  cuFileHandleDeregister(cf_handle);
  close(fd);
}

ssize_t GPUFileHandle::readData(FITSDataGPU &buff, const size_t nbytes,
                                off_t file_off, off_t device_off) {
  // file.readData(data, fits.spat_size, channel * fits.spat_size, 0);
  file_off = file_off * sizeof(float) + data_position;
  return cuFileRead(cf_handle, thrust::raw_pointer_cast(buff.raw.data()),
                    nbytes * sizeof(float), file_off, device_off);
}

template <typename T> struct endian_swap_op;
struct replace_nan_op;

} // namespace fits
