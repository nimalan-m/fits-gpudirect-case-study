#include <algorithm>
#include "fits-io.hh"

// TODO: Add google test
static std::string content_1 =
    "NAME      MARK                                                          "
    "        TEST      ASDAA                                                 "
    "                SIZEX     1232                                          "
    "                        SIZEY     4567                                  "
    "                                ";

static std::string content_2 = "NEW_KEY   MARK                                 "
                               "                                 ";

void assert_header_should_be_able_to_parse_header_and_fetch_int_value() {
  fits::FITSHeader h(content_1);
  long actual = h.get_int("SIZEX");
  long expected = 1232;
  if (actual != expected)
    std::cout << "Expected: " << expected << " Actual: " << actual << std::endl;
  assert(expected == actual);
}

void assert_header_should_be_able_to_parse_header_and_fetch_value() {
  fits::FITSHeader h(content_1);
  std::string actual = h.get("NAME");
  std::string expected = "MARK";
  if (actual != expected)
    std::cout << "Expected: " << expected << " Actual: " << actual << std::endl;
  assert(expected == actual);
}

void assert_header_should_be_able_to_check_if_key_exists() {
  fits::FITSHeader h(content_1);
  bool actual = h.find("UNKNOWN_KEY");
  bool expected = false;
  if (actual != expected)
    std::cout << "Expected: " << expected << " Actual: " << actual << std::endl;
  assert(expected == actual);
}

void assert_header_should_be_able_to_merge() {
  fits::FITSHeader h1(content_1);
  fits::FITSHeader h2(content_2);

  h1.merge(h2);
  bool actual = h1.find("NEW_KEY");
  bool expected = true;
  if (actual != expected)
    std::cout << "Expected: " << expected << " Actual: " << actual << std::endl;
  assert(expected == actual);
}

int main() {
  (void)"tests for headers";
  {
    assert_header_should_be_able_to_parse_header_and_fetch_int_value();
    assert_header_should_be_able_to_parse_header_and_fetch_value();
    assert_header_should_be_able_to_check_if_key_exists();
    assert_header_should_be_able_to_merge();
  }
  std::cout << "Assertions passed" << std::endl;
}
