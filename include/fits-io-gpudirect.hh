#pragma once

#include <cufile.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>

#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>

namespace fits {
class FITSDataGPU {
public:
  size_t spat_size;
  size_t num_channels;
  size_t total_pix;
  bool pin_buffer;

  thrust::device_vector<float> raw;

  FITSDataGPU() = delete;
  FITSDataGPU(size_t spat_size, size_t num_channels, bool pin_buffer)
      : spat_size(spat_size), num_channels(num_channels),
        total_pix(spat_size * num_channels), pin_buffer(pin_buffer),
        raw(total_pix) {
    if (pin_buffer)
      register_cubuffer();
  }
  FITSDataGPU(size_t size, bool pin_buffer)
      : FITSDataGPU(size, 1, pin_buffer) {}
  ~FITSDataGPU() {
    if (pin_buffer)
      deregister_cubuffer();
  }

  CUfileError_t register_cubuffer() {
    return cuFileBufRegister(thrust::raw_pointer_cast(raw.data()),
                             total_pix * sizeof(float), 0);
  }

  CUfileError_t deregister_cubuffer() {
    return cuFileBufDeregister(thrust::raw_pointer_cast(raw.data()));
  }
};

// FIXME: Revisit object lifecycle
class GPUFileHandle {
  CUfileError_t status;
  int fd;
  CUfileDescr_t cf_desc;
  CUfileHandle_t cf_handle;
  std::string filename;
  std::ios::pos_type data_position;

public:
  GPUFileHandle(std::string filename, std::ios::pos_type data_position);
  ~GPUFileHandle();
  ssize_t readData(FITSDataGPU &buff, const size_t nbytes, off_t file_off,
                   off_t device_off);
};

template <typename T>
struct endian_swap_op : public thrust::unary_function<T, T> {
  __host__ __device__ T operator()(T x) {
    uint x_i = reinterpret_cast<::std::uint32_t const &>(x);
    x_i = (((x_i & 0x000000FF) << 24) | ((x_i & 0x0000FF00) << 8) |
           ((x_i & 0x00FF0000) >> 8) | ((x_i & 0xFF000000) >> 24));
    return reinterpret_cast<T const &>(x_i);
  }
};

} // namespace fits