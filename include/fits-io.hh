#pragma once

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <cstring>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <string>
#include <vector>

namespace fits {

static const size_t FITS_HEADER_BLOCK_SIZE = 2880;
static const size_t FITS_HEADER_LINE_SIZE = 80;
static const size_t FITS_HEADER_KEY_SIZE = 10;
static const size_t FITS_HEADER_VALUE_SIZE = 70;

static inline void rtrim(std::string &s) {
  s.erase(std::find_if(s.rbegin(), s.rend(),
                       [](unsigned char ch) { return !std::isspace(ch); })
              .base(),
          s.end());
}

static inline bool is_little_endian(void) {
  const unsigned int n = 1U;
  return *((unsigned char *)&n) == 1U;
}

// TODO: This is not needed
inline void data_swap_byte_order_float(float *raw, size_t size) {
  if (is_little_endian()) {
    // printf("Little endian conversion\n");

    // TODO: Use reinterpret_cast
    uint32_t *ptr = (uint32_t *)raw;

#pragma omp parallel for schedule(static)
    for (ssize_t i = 0; i < size; i += 1) {
      ptr[i] = __builtin_bswap32(ptr[i]);
    }
  }

  return;
}

static inline void swap_byte_order(char *word, const size_t size) {
  if (size == 2)
    *((uint16_t *)word) = __builtin_bswap16(*((uint16_t *)word));
  else if (size == 4)
    *((uint32_t *)word) = __builtin_bswap32(*((uint32_t *)word));
  else if (size == 8)
    *((uint64_t *)word) = __builtin_bswap64(*((uint64_t *)word));

  return;
}

class FITSData {
public:
  size_t spat_size;
  size_t num_channels;
  size_t total_pix;

  std::vector<float> raw;
  FITSData() = delete;
  FITSData(size_t size)
      : spat_size(size), num_channels(1), total_pix(size), raw(size) {}
  FITSData(size_t spat_size, size_t num_channels)
      : spat_size(spat_size), num_channels(num_channels),
        total_pix(spat_size * num_channels), raw(spat_size * num_channels) {}

  inline void data_swap_byte_order() {
    if (is_little_endian()) {
      // printf("Little endian conversion\n");

      // TODO: Use reinterpret_cast
      uint32_t *ptr = (uint32_t *)raw.data();

#pragma omp parallel for schedule(static)
      for (ssize_t i = 0; i < total_pix; i += 1) {
        ptr[i] = __builtin_bswap32(ptr[i]);
      }
    }

    return;
  }
};

class FITSHeader {
  std::map<std::string, std::string> headers;

public:
  FITSHeader() {}
  FITSHeader(char *content, size_t size) {
    char *new_content = new char[size + 1];
    std::memcpy(new_content, content, size);
    new_content[size] = '\0';

    std::string str_content(new_content);
    parse_content(str_content);

    delete[] new_content;
  }
  FITSHeader(std::string &content) { parse_content(content); }

  void parse_content(std::string &content) {
    assert(content.size() % FITS_HEADER_LINE_SIZE == 0);
    for (size_t offset = 0; offset < content.size();
         offset += FITS_HEADER_LINE_SIZE) {
      std::string line = content.substr(offset, FITS_HEADER_LINE_SIZE);
      std::string key = line.substr(0, FITS_HEADER_KEY_SIZE);

      rtrim(key);
      // TODO: Review if this '=' logic is in FITS standard
      auto idx = key.find('=');
      if (idx != std::string::npos) {
        key = key.substr(0, idx);
        rtrim(key);
      }

      std::string val =
          line.substr(FITS_HEADER_KEY_SIZE, FITS_HEADER_VALUE_SIZE);
      rtrim(val);

      headers[key] = val;
    }
  }

  // inline std::map<std::string, std::string>::iterator fetch_key(const
  // std::string &key) {
  //   auto v = headers.find(key);
  //   if (v != headers.end())
  //     return v;
  // }

  bool find(const std::string &key) {
    auto v = headers.find(key);
    return v != headers.end();
  }

  std::string get(const std::string &key) {
    auto v = headers.find(key);
    if (v != headers.end()) {
      return v->second;
    } else {
      return "";
    }
  }

  long get_int(const std::string &key) {
    auto v = headers.find(key);
    if (v != headers.end()) {
      return std::atol(v->second.c_str());
    } else {
      return 0;
    }
  }

  void merge(FITSHeader &other) {
    headers.insert(other.headers.begin(), other.headers.end());
  }

  void print_headers() {
    for (auto v : headers) {
      std::cout << v.first << "\t" << v.second << std::endl;
    }
  }
};

class FITS {
  std::string filename;
  FITSHeader headers;
  std::ifstream file;
  size_t dim;
  size_t naxes[4];

public:
  size_t spat_size;
  size_t spec_size;
  size_t totalpix;
  std::ios::pos_type data_position;

  FITS() = delete;
  FITS(const std::string &filename)
      : filename(filename), file(filename, std::ios::binary) {
    assert(file.is_open());

    bool end_reached = false;
    char *fits_line = new char[FITS_HEADER_BLOCK_SIZE];

    while (!end_reached) {
      if (file.read(fits_line, FITS_HEADER_BLOCK_SIZE)) {
        FITSHeader block(fits_line, FITS_HEADER_BLOCK_SIZE);
        end_reached = block.find("END");
        headers.merge(block);
      } else {
        break;
      }
    }
    delete[] fits_line;

    data_position = file.tellg();

    dim = headers.get_int("NAXIS");
    naxes[0] = headers.get_int("NAXIS1");
    naxes[1] = headers.get_int("NAXIS2");
    naxes[2] = headers.get_int("NAXIS3");
    naxes[3] = headers.get_int("NAXIS4");

    if (dim != 4)
      std::cerr << "Image has to be 4D [X] [Y] [Freq] [Polarisation]"
                << std::endl;
    assert(dim == 4);

    spat_size = naxes[0] * naxes[1];

    totalpix = naxes[0] * naxes[1] * naxes[2] * naxes[3];

    const int data_type = headers.get_int("BITPIX");
    int word_size = abs(data_type / 8);

    if (naxes[2] == 1) {
      size_t temp = naxes[2];
      naxes[3] = naxes[2];
      naxes[2] = temp;
    }

    if (naxes[3] != 1)
      std::cerr << "Polarisation axis must be one" << std::endl;
    assert(naxes[3] == 1);

    spec_size = naxes[2];
  }

  // TODO: Do I want 1 indexed channel??
  FITSData read_channel(size_t channel) {
    assert(channel < spec_size);

    std::ios::pos_type beg = data_position * channel;
    std::ios::pos_type end = data_position * (channel + 1);
    // std::ios::pos_type end = beg + (spat_size * sizeof(float));

    // std::istream_iterator<float>(beg)

    // std::cout << file.fail() << std::endl;
    file.seekg(beg, std::ios::beg);
    assert(!file.fail());

    FITSData fits_data(spat_size);

    std::vector<float> v(beg, end);
    std::cout << "Size: " << v.size() << std::endl;
    // data_swap_byte_order_float(v.data(), v.size());
    for (int i = 0; i < 10; i++) {
      std::cout << v[i] << " ";
    }
    std::cout << std::endl;

    // fits_data.raw = std::move(v);

    // TODO: Error handling?
    // file.read((char *)fits_data.raw.data(), spat_size * sizeof(float));
    fits_data.raw.insert(fits_data.raw.begin(),
                         std::istream_iterator<char>(file),
                         std::istream_iterator<char>());
    // fits_data.data_swap_byte_order();

    for (int i = 0; i < 10; i++) {
      std::cout << fits_data.raw[i] << " ";
    }
    std::cout << std::endl;

    return fits_data;
  }

  void print_info(bool print_headers = false) {
    if (print_headers)
      headers.print_headers();
    std::cout << "Image resolution: " << naxes[0] << "x" << naxes[1] << "x"
              << naxes[2] << "x" << naxes[3] << std::endl;
    std::cout << "Total pixels " << totalpix << std::endl;
    std::cout << "Data pos: " << data_position << std::endl;
  }
};

} // namespace fits
